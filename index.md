# Configurando una interfaz Ethernet como enlace VLAN trunk 802.1q en sistemas GNU/Linux

## Introducción

En los entornos de red actuales es necesario que las herramientas tecnologicas que usemos para
administrar la red sean flexibles y se adapten a los cambios dinámicos del crecimiento de la red,
una de las técnicas más usadas en la administración de redes es la segmentación de grupos de
trabajo de red en diferentes dominios de broadcast, normalmente cada grupo de trabajo conectado
a un switch físico independiente y para que las redes separadas se puedan comunicar se utilizan
routers.

En la actualidad los switches *administrables* permite la separación de múltiples redes LAN de
forma lógica, es decir, dentro de un mismo dispositivo físico es posible crear múltiples redes
**LAN Virtuales** ó **VLANs**, básicamente los paquetes son separados y distinguidos por una
etiqueta en la trama Ethernet, el protocolo **IEEE 802.1q** es el estandar que permite el
etiquetado de tramás, en los switches se asignan las etiquetas de la VLAN a los puertos, hay
puertos en modo acceso los cuales permiten conectar un sistema de forma transparente y el switch
realiza el etiquetado con el ID de la VLAN asignado, también existen los puertos troncales o port
trunks los cuales tienen la capacidad de transportar tráfico de red de diferentes VLANs,
normalmente este es un puerto Ethernet con soporte VLAN 802.1q que a través de un driver del SO
permite la creación de sub interfaces de red en las cuales se etiqueta el tráfico con el ID de
las VLAN configuradas. Los puertos trunk son normalmente utilizados en routers de VLANs los cuales
permiten la comunicación entre las diferentes redes lógicas, el enrutado se realiza en la capa 3
del modelo OSI, en este caso a nivel IP. Hasta hace poco estos routers de VLAN eran dispositivos
de red *especializados* de *marca* y con sistemas operativos privativos, aquí les vengo a mostrar
como configurar un sistema GNU/Linux con soporte VLAN 802.1q para realizar el enrutado de
múltiples redes VLAN.

En este documento se describen los procedimientos para instalar y configurar el soporte VLAN
**IEEE 802.1a** en sistemas GNU/Linux, veremos como configurar un enlace troncal *trunk link*
y permitir la multiplexión de varias VLAN a través de un solo enlace, en este caso una interfaz
Ethernet. La separación lógica se realiza en un switch Layer 2 con soporte VLAN y la comunicación
entre las VLANs se realiza a través de un router con soporte VLAN.

La implementación del router la realizaremos en un sistema GNU/Linux, específicamente la
distribución **Rasberry Pi OS**, aunque en este documento solo se describen los pasos para
configurar el enrutado básico es importante hacer saber que el kernel Linux incluye el soporte
para realizar varias operaciones relacionadas al tráfico de red, por ejemplo, NAT, Stateful Packet
Filtering y QoS entre otras cosas.

La configuración aquí descrita servirá como la base de una instalación de red simple, solo use
este ejemplo como base para la implementación de acuerdo a los requerimientos de su entorno de red.

## Descripción de la red basada en VLANs y router GNU/Linux

El esquema de red basado en VLANs propone la separación de la red de la empresa ficticia **Example**
en diferentes subredes aisladas una de otra, la separación será lógica, agruparemos diferentes
grupos de trabajo, comunidades de usuarios o aplicaciones en diferentes redes LAN virtuales ó VLAN.

Cada red tendrá asignado una subred IP diferente, esto permitirá tener mayor control en el tráfico
y una reducción de tráfico broadcast en la red. El tráfico entre las diferentes subredes será
enrutado a través de un router con soporte VLAN trunk 802.1q.

Las VLANs y subredes IP se describen en la siguiente tabla:

| VLAN |       Descripción       |      Dirección IP       |
| ---- | :---------------------: | :---------------------: |
| 100  | Administración Routers  | 192.168.100.0/24        |
| 110  | Administración switches | 192.168.110.0/24        |
| 120  |     Red Servidores      | 192.168.120.0/24        |
| 130  |  Red PCs e Impresoras   | 192.168.130.0/24        |
| 140  |     Red equipo VoIP     | 192.168.140.0/24        |
| 150  |        Red WIFI         | 192.168.150.0/24        |

La separación lógica basada en VLANs se realiza en el switch de nombre **switch01** de marca y
modelo Aruba Instant On 1930, en la siguiente tabla se muestra su descripción:

| Nombre               | Modelo                | MAC Address    | Dirección IP  | Ubicación       |
| -------------------- | --------------------- | ---------------| --------------| ----------------|
| switch01.example.com | Aruba Instant On 1930 | 0024-7386-9685 | 192.168.110.1 | SITE01, RACK014 |

El puerto GE1/0/1 del switch SWITCH01 esta configurado en modo hybrido trunk 802.1p para las VLANs
100, 110, 120, 130 y 140.

El tráfico entre las VLANs será controlado a través de un Router GNU/Linux que está conectado al
switch en el puerto GE1/0/1 a través de una interfaz Ethernet Gigabit, dicha interfaz también esta
configurada en modo trunk 802.1q.

## Configuración de VLANs y puertos en Switch administrable Aruba

En esta sección se describe brevemente la configuración del switch en respecto a las VLANs y los
puertos, de acuerdo a los requerimientos se crearon las siguientes VLANs:

| VLAN ID | Descripción  |
| :-----: | :----------: |
|    1    | VLAN DEFAULT |
|   100   | VLAN ROUTING |
|   110   | VLAN SWITCH  |
|   120   | VLAN SERVERS |
|   130   | VLAN PCS     |
|   140   | VLAN VOIP    |
|   150   | VLAN WIFI    |

La asignación de puertos a VLAN se describe en la siguiente tabla:

| VLAN ID | Descipción   | Miembros sin etiqueta | Miembros con etiqueta     |
| :-----: | :----------: | :-------------------: | :-----------------------: |
|   100   | VLAN ROUTING |                       | GE1/0/1                   |
|   110   | VLAN SWITCH  |                       | GE1/0/1                   |
|   120   | VLAN SERVERS | GE1/0/9-GE1/0/16      | GE1/0/1                   |
|   130   | VLAN PCS     | GE1/0/17-GE1/0/24     | GE1/0/1                   |
|   140   | VLAN VOIP    |                       | GE1/0/1,GE1/0/17-GE1/0/24 |
|   150   | VLAN WIFI    |                       | GE1/0/8                   |

La VLAN de administración o *VLAN Interfaz* es una VLAN que se usa para acceder a la interfaz de
administración del switch. Se asigno la *VLAN 110* al puerto *GE1/0/1* y se le asignó la
dirección IP 192.168.100.1.

Guarde y respalde la configuración actual.

## Configurando los requerimientos para interfaces VLAN en GNU/Linux

Para configurar interfaces VLAN 802.1q debe ejecutar un kernel con soporte VLAN 802.1q, una
forma de saber si el kernel que estamos ejecutando incluye el soporte del protocolo 802.1q
es cargando el módulo `8021q` con el comando `modprobe`:

```
# modprobe 8021q
```

Al cargar el modulo registra un evento de kernel que puede ser visto con `dmesg`:


```
# dmesg | grep -i 802.1q
[   11.052753] 8021q: 802.1Q VLAN Support v1.8
```

El mensaje anterior nos indica que el soporte de VLAN esta disponible en el sistema, también puede
verificar si el módulo en realidad esta cargado usando el comando `lsmod`.

```
# lsmod | grep 8021q
8021q                  32768  0
garp                   16384  1 8021q
```

Cuando se carga el modulo del kernel 8021q se crea el archivo de parámetros del kernel
`/proc/net/vlan/config`, el cual luce así:

```
# cat /proc/net/vlan/config
VLAN Dev name    | VLAN ID
Name-Type: VLAN_NAME_TYPE_RAW_PLUS_VID_NO_PAD
```

En el directorio ` /proc/net/vlan/  `habrá un archivo de para cada VLAN creada que tendrá
información como el VLAN ID, estadísticas y otros.

Para asegurarse de que el modulo 8021q sea cargado automáticamente al inicio del sistema agreguelo
al archivo `/etc/modules`, por ejemplo:

```
# echo 8021q >> /etc/modules
```

El sistema también debe contar con la herramienta *userspace* de configuración de interfaces VLAN
`vconfig`, en Debian/Ubuntu se instala con el paquete *vlan*, el cual no viene en una instalación
de Debian/Ubuntu por defecto, por lo que hay que instalarlo manualmente.

```
# apt-get update && apt-get install vlan
```

En la siguiente sección veremos como crear las interfaces VLAN en la interfaz de red eth0, las
interfaces VLAN básicamente son subinterfaces de la interfaz física o raw eth0.

## Configuración de interfaz VLAN 802.1q en Raspberry Pi OS

Como mencionamos arriba, para configurar una o más interfaz VLAN 802.1q en sistemas GNU/Linux se
requiere de una interfaz Ethernet con soporte VLAN 802.1q, en especifico el driver con soporte
VLAN, las interfaces VLAN en Linux son algo así como subinterfaces asociadas a una interfaz
*RAW*, en este caso la interfaz física Ethernet.

La configuración de las VLANs en el servidor Raspberry Pi OS las vamos a realizar en un equipo con
las siguientes especificaciones:

|   Componente   | Descripción                 |
| :------------: | :-------------------------: |
|      CPU       | ARMv7 Processor rev 4 (v7l) |
|    Memoria     | 1GB                         |
| Red (eth0)     | Integrada                   |

**NOTA:** El driver de la interfaz de red debe soportar el soporte VLAN, actualmente la mayoría
de interfaces de red en Linux soportan VLAN.

Aunque es posible configurar las VLANs y las interfaces de red virtuales de forma manual usando
los comandos `vconfig`, `ifconfig` o `ip` en este documento vamos a usar las herramientas de
configuración de red del sistema operativo, en distribuciones Debian/Ubuntu podemos configurar
las interfaces VLAN desde el archivo de configuración `/etc/network/interfaces`.

Edite el archivo `/etc/network/intefaces` para definir las VLANs y las direcciones IP asociadas
a cada interfaz virtual:

```
# vim /etc/network/interfaces
```

Agreguemos las siguientes líneas para configurar la interfaz física eth0 en modo `manual` y
definimos las interfaces de red virtuales VLAN asociadas a la interfaz *raw* eth0.

```
# Interfaz VLAN 802.1q (Trunk)
auto eth0
iface eth0 inet manual

# Interfaz VLAN 100 - ROUTING
auto vlan100
iface vlan100 inet static
  address 192.168.100.254
  netmask 255.255.255.0
  vlan_raw_device eth0

# Interfaz VLAN 110 - SWITCH
auto vlan110
iface vlan110 inet static
  address 192.168.110.254
  netmask 255.255.255.0
  vlan_raw_device eth0

# Interfaz VLAN 120 - SERVERS
auto vlan120
iface vlan120 inet static
  address 192.168.120.254
  netmask 255.255.255.0
  vlan_raw_device eth0

# Interfaz VLAN 130 - PCS
auto vlan130
iface vlan130 inet static
  address 192.168.130.254
  netmask 255.255.255.0
  vlan_raw_device eth0

# Interfaz VLAN 140 - VOIP
auto vlan140
iface vlan140 inet static
  address 192.168.140.254
  netmask 255.255.255.0
  vlan_raw_device eth0

# Interfaz VLAN 150 - WIFI
auto vlan150
iface vlan150 inet static
  address 192.168.150.254
  netmask 255.255.255.0
  vlan_raw_device eth0

```

**NOTA:** Cuando se configura una interfaz en modo manual no se requiere definir una dirección
IP estática.

Active las interfaces VLAN de forma independiente usando el comando `ifup`:

```
# ifup vlan100
Set name-type for VLAN subsystem. Should be visible in /proc/net/vlan/config
Added VLAN with VID == 100 to IF -:eth0:-
```

```
# ifup vlan110
Set name-type for VLAN subsystem. Should be visible in /proc/net/vlan/config
Added VLAN with VID == 110 to IF -:eth0:-
```

```
# ifup vlan120
Set name-type for VLAN subsystem. Should be visible in /proc/net/vlan/config
Added VLAN with VID == 120 to IF -:eth0:-
```

```
# ifup vlan130
Set name-type for VLAN subsystem. Should be visible in /proc/net/vlan/config
Added VLAN with VID == 130 to IF -:eth0:-
```

```
# ifup vlan140
Set name-type for VLAN subsystem. Should be visible in /proc/net/vlan/config
Added VLAN with VID == 140 to IF -:eth0:-
```

```
# ifup vlan150
Set name-type for VLAN subsystem. Should be visible in /proc/net/vlan/config
Added VLAN with VID == 150 to IF -:eth0:-
```

Para desactivar las interfaces VLAN puede usar el comando `ifdown`, por ejemplo:

```
# ifdown vlan150
```

Para reiniciar una interfaz se recomienda que utilice:

```
# ifdown vlan150 && ifup vlan150
```

Cuando se crean las VLANs podemos ver la siguiente información en el archivo de parámetros del
kernel `/proc/net/vlan/config`:

```
# cat /proc/net/vlan/config
VLAN Dev name    | VLAN ID
Name-Type: VLAN_NAME_TYPE_PLUS_VID_NO_PAD
vlan100        | 100  | eth0
vlan110        | 110  | eth0
vlan120        | 120  | eth0
vlan130        | 130  | eth0
vlan140        | 140  | eth0
vlan150        | 150  | eth0
```

Para ver más información de las interfaces virtuales vea el archivo correspondiente a la VLAN
en el directorio `/proc/net/vlan`, por ejemplo:

```
# cat /proc/net/vlan/vlan100
vlan100  VID: 100        REORDER_HDR: 1  dev->priv_flags: 1
         total frames received            0
          total bytes received            0
      Broadcast/Multicast Rcvd            0

      total frames transmitted           42
       total bytes transmitted        12112
            total headroom inc            0
           total encap on xmit           42
Device: eth0
INGRESS priority mappings: 0:0  1:0  2:0  3:0  4:0  5:0  6:0 7:0
 EGRESS priority mappings:
```

```
# cat /proc/net/vlan/vlan110
vlan110  VID: 110        REORDER_HDR: 1  dev->priv_flags: 1
         total frames received            0
          total bytes received            0
      Broadcast/Multicast Rcvd            0

      total frames transmitted           34
       total bytes transmitted        11119
            total headroom inc            0
           total encap on xmit           34
Device: eth0
INGRESS priority mappings: 0:0  1:0  2:0  3:0  4:0  5:0  6:0 7:0
 EGRESS priority mappings:
```

```
# cat /proc/net/vlan/vlan120
vlan120  VID: 120        REORDER_HDR: 1  dev->priv_flags: 1
         total frames received            0
          total bytes received            0
      Broadcast/Multicast Rcvd            0

      total frames transmitted           37
       total bytes transmitted        11621
            total headroom inc            0
           total encap on xmit           37
Device: eth0
INGRESS priority mappings: 0:0  1:0  2:0  3:0  4:0  5:0  6:0 7:0
 EGRESS priority mappings:
```

```
# cat /proc/net/vlan/vlan130
vlan130  VID: 130        REORDER_HDR: 1  dev->priv_flags: 1
         total frames received            0
          total bytes received            0
      Broadcast/Multicast Rcvd            0

      total frames transmitted           35
       total bytes transmitted        11286
            total headroom inc            0
           total encap on xmit           35
Device: eth0
INGRESS priority mappings: 0:0  1:0  2:0  3:0  4:0  5:0  6:0 7:0
 EGRESS priority mappings:
```

```
# cat /proc/net/vlan/vlan140
vlan140  VID: 140        REORDER_HDR: 1  dev->priv_flags: 1
         total frames received            0
          total bytes received            0
      Broadcast/Multicast Rcvd            0

      total frames transmitted           36
       total bytes transmitted        11607
            total headroom inc            0
           total encap on xmit           36
Device: eth0
INGRESS priority mappings: 0:0  1:0  2:0  3:0  4:0  5:0  6:0 7:0
 EGRESS priority mappings:
```

```
# cat /proc/net/vlan/vlan150
vlan150  VID: 150        REORDER_HDR: 1  dev->priv_flags: 1
         total frames received            0
          total bytes received            0
      Broadcast/Multicast Rcvd            0

      total frames transmitted           25
       total bytes transmitted         4587
            total headroom inc            0
           total encap on xmit           25
Device: eth0
INGRESS priority mappings: 0:0  1:0  2:0  3:0  4:0  5:0  6:0 7:0
 EGRESS priority mappings:
```

Puede usar los comandos `ifconfig` o `ip` para ver la información de las interfaces de red
físicas y virtuales y la dirección IP asignada, por ejemplo:

```
eth0      Link encap:Ethernet  direcciónHW 00:e0:29:5b:ae:a4
          Dirección inet6: fe80::2e0:29ff:fe5b:aea4/64 Alcance:Enlace
          ACTIVO DIFUSIÓN FUNCIONANDO MULTICAST  MTU:1500  Métrica:1
          Paquetes RX:0 errores:0 perdidos:0 overruns:0 frame:0
          Paquetes TX:151 errores:0 perdidos:0 overruns:0 carrier:0
          colisiones:0 long.colaTX:1000
          Bytes RX:0 (0.0 B)  TX bytes:23648 (23.6 KB)
          Interrupción:21 Dirección base: 0xe800

vlan100   Link encap:Ethernet  direcciónHW 00:e0:29:5b:ae:a4
          Direc. inet:192.168.100.254  Difus.:192.168.100.255  Másc:255.255.255.0
          Dirección inet6: fe80::2e0:29ff:fe5b:aea4/64 Alcance:Enlace
          ACTIVO DIFUSIÓN FUNCIONANDO MULTICAST  MTU:1500  Métrica:1
          Paquetes RX:0 errores:0 perdidos:0 overruns:0 frame:0
          Paquetes TX:29 errores:0 perdidos:0 overruns:0 carrier:0
          colisiones:0 long.colaTX:0
          Bytes RX:0 (0.0 B)  TX bytes:4632 (4.6 KB)

vlan110   Link encap:Ethernet  direcciónHW 00:e0:29:5b:ae:a4
          Direc. inet:192.168.110.254  Difus.:192.168.110.255  Másc:255.255.255.0
          Dirección inet6: fe80::2e0:29ff:fe5b:aea4/64 Alcance:Enlace
          ACTIVO DIFUSIÓN FUNCIONANDO MULTICAST  MTU:1500  Métrica:1
          Paquetes RX:0 errores:0 perdidos:0 overruns:0 frame:0
          Paquetes TX:29 errores:0 perdidos:0 overruns:0 carrier:0
          colisiones:0 long.colaTX:0
          Bytes RX:0 (0.0 B)  TX bytes:4632 (4.6 KB)

vlan120   Link encap:Ethernet  direcciónHW 00:e0:29:5b:ae:a4
          Direc. inet:192.168.120.254  Difus.:192.168.120.255  Másc:255.255.255.0
          Dirección inet6: fe80::2e0:29ff:fe5b:aea4/64 Alcance:Enlace
          ACTIVO DIFUSIÓN FUNCIONANDO MULTICAST  MTU:1500  Métrica:1
          Paquetes RX:0 errores:0 perdidos:0 overruns:0 frame:0
          Paquetes TX:29 errores:0 perdidos:0 overruns:0 carrier:0
          colisiones:0 long.colaTX:0
          Bytes RX:0 (0.0 B)  TX bytes:4632 (4.6 KB)

vlan130   Link encap:Ethernet  direcciónHW 00:e0:29:5b:ae:a4
          Direc. inet:192.168.130.254  Difus.:192.168.130.255  Másc:255.255.255.0
          Dirección inet6: fe80::2e0:29ff:fe5b:aea4/64 Alcance:Enlace
          ACTIVO DIFUSIÓN FUNCIONANDO MULTICAST  MTU:1500  Métrica:1
          Paquetes RX:0 errores:0 perdidos:0 overruns:0 frame:0
          Paquetes TX:29 errores:0 perdidos:0 overruns:0 carrier:0
          colisiones:0 long.colaTX:0
          Bytes RX:0 (0.0 B)  TX bytes:4632 (4.6 KB)

vlan140   Link encap:Ethernet  direcciónHW 00:e0:29:5b:ae:a4
          Direc. inet:192.168.140.254  Difus.:192.168.140.255  Másc:255.255.255.0
          Dirección inet6: fe80::2e0:29ff:fe5b:aea4/64 Alcance:Enlace
          ACTIVO DIFUSIÓN FUNCIONANDO MULTICAST  MTU:1500  Métrica:1
          Paquetes RX:0 errores:0 perdidos:0 overruns:0 frame:0
          Paquetes TX:29 errores:0 perdidos:0 overruns:0 carrier:0
          colisiones:0 long.colaTX:0
          Bytes RX:0 (0.0 B)  TX bytes:4632 (4.6 KB)

vlan150   Link encap:Ethernet  direcciónHW 00:e0:29:5b:ae:a4
          Direc. inet:192.168.150.254  Difus.:192.168.150.255  Másc:255.255.255.0
          Dirección inet6: fe80::2e0:29ff:fe5b:aea4/64 Alcance:Enlace
          ACTIVO DIFUSIÓN FUNCIONANDO MULTICAST  MTU:1500  Métrica:1
          Paquetes RX:0 errores:0 perdidos:0 overruns:0 frame:0
          Paquetes TX:25 errores:0 perdidos:0 overruns:0 carrier:0
          colisiones:0 long.colaTX:0
          Bytes RX:0 (0.0 B)  TX bytes:4587 (4.5 KB)
```

También puede usar el comando `ip` para ver las interfaces:

```
# ip link list
3: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UNKNOWN qlen 1000
    link/ether 00:e0:29:5b:ae:a4 brd ff:ff:ff:ff:ff:ff
4: vlan100@eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP
    link/ether 00:e0:29:5b:ae:a4 brd ff:ff:ff:ff:ff:ff
5: vlan110@eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP
    link/ether 00:e0:29:5b:ae:a4 brd ff:ff:ff:ff:ff:ff
6: vlan120@eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP
    link/ether 00:e0:29:5b:ae:a4 brd ff:ff:ff:ff:ff:ff
7: vlan130@eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP
    link/ether 00:e0:29:5b:ae:a4 brd ff:ff:ff:ff:ff:ff
8: vlan140@eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP
    link/ether 00:e0:29:5b:ae:a4 brd ff:ff:ff:ff:ff:ff
9: vlan150@eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP
    link/ether 00:e0:29:5b:ae:a4 brd ff:ff:ff:ff:ff:ff
```

**NOTA:** En esta salida es posible ver la interfaz física a la que esta amarrada la VLAN.

Y para ver las direcciones IP asignadas a las interfaces VLAN:

```
# ip addr list
3: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UNKNOWN qlen 1000
    link/ether 00:e0:29:5b:ae:a4 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::2e0:29ff:fe5b:aea4/64 scope link
       valid_lft forever preferred_lft forever
4: vlan100@eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP
    link/ether 00:e0:29:5b:ae:a4 brd ff:ff:ff:ff:ff:ff
    inet 192.168.100.254/24 brd 192.168.100.255 scope global vlan100
    inet6 fe80::2e0:29ff:fe5b:aea4/64 scope link
       valid_lft forever preferred_lft forever
5: vlan110@eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP
    link/ether 00:e0:29:5b:ae:a4 brd ff:ff:ff:ff:ff:ff
    inet 192.168.110.254/24 brd 192.168.110.255 scope global vlan110
    inet6 fe80::2e0:29ff:fe5b:aea4/64 scope link
       valid_lft forever preferred_lft forever
6: vlan120@eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP
    link/ether 00:e0:29:5b:ae:a4 brd ff:ff:ff:ff:ff:ff
    inet 192.168.120.254/24 brd 192.168.120.255 scope global vlan120
    inet6 fe80::2e0:29ff:fe5b:aea4/64 scope link
       valid_lft forever preferred_lft forever
7: vlan130@eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP
    link/ether 00:e0:29:5b:ae:a4 brd ff:ff:ff:ff:ff:ff
    inet 192.168.130.254/24 brd 192.168.130.255 scope global vlan130
    inet6 fe80::2e0:29ff:fe5b:aea4/64 scope link
       valid_lft forever preferred_lft forever
8: vlan140@eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP
    link/ether 00:e0:29:5b:ae:a4 brd ff:ff:ff:ff:ff:ff
    inet 192.168.140.254/24 brd 192.168.140.255 scope global vlan140
    inet6 fe80::2e0:29ff:fe5b:aea4/64 scope link
       valid_lft forever preferred_lft forever
9: vlan150@eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP
    link/ether 00:e0:29:5b:ae:a4 brd ff:ff:ff:ff:ff:ff
    inet 192.168.150.254/24 brd 192.168.150.255 scope global vlan150
    inet6 fe80::2e0:29ff:fe5b:aea4/64 scope link
       valid_lft forever preferred_lft forever
```

**NOTA:** La interfaz física eth0 no tiene una dirección IP asignada ya que para este escenario
no se necesita.

Veamos la tabla de rutas en el servidor router:

```
# route -n
Tabla de rutas IP del núcleo
Destino         Pasarela        Genmask         Indic Métric Ref    Uso Interfaz
192.168.100.0   0.0.0.0         255.255.255.0   U     0      0        0 vlan100
192.168.110.0   0.0.0.0         255.255.255.0   U     0      0        0 vlan110
192.168.120.0   0.0.0.0         255.255.255.0   U     0      0        0 vlan120
192.168.130.0   0.0.0.0         255.255.255.0   U     0      0        0 vlan130
192.168.140.0   0.0.0.0         255.255.255.0   U     0      0        0 vlan140
192.168.150.0   0.0.0.0         255.255.255.0   U     0      0        0 vlan150
```

**NOTA:** De ahora en adelante, tendrá que configurar las interfaces virtuales, es decir,
con las subinterfaces no la física eth0, es decir las interfaces *vlanXXX*.

**NOTA:** Para convertir al servidor en un router debe permitir el reenvío de paquetes IP entre
las diferentes interfaces de red, en especifico, debe habilitar el soporte *IP Forwarding* en el
kernel, para controlar el IP forwarding en el sistema cambie el estado del archivo
`/proc/sys/net/ipv4/ip_forward`, por ejemplo:

```
# echo 1 > /proc/sys/net/ipv4/ip_forward
```

Verifique el estado del IP forwarding:

```
# cat /proc/sys/net/ipv4/ip_forward
1
```

**NOTA:** Para hacer persistente este cambio agregue la línea **net.ipv4.ip_forward=1**
al archivo `/etc/sysctl.conf`.

**NOTA:** Si desea configurar una interfaz debe ser sobre la interfaz vlanXXX, o si desea
analizar el tráfico con alguna herramienta como `tcpdump`, tshark o wireshark siga las
siguientes recomendaciones, por ejemplo usando tcpdump:

Para analizar el tráfico dentro de una VLAN en el router use:

```
# tcpdump -nvi vlan130
```

Para analizar el tráfico de todas las VLANs use:

```
# tcpdump -i eth0
```

En la siguiente sección veremos algunas pruebas de conectividad entre diferentes dispositivos
conectados en diferentes VLANs.

## Pruebas de conectividad entre VLANs

Realice pruebas de conectividad usando el comando ping, primero pruebe con tráfico originado en el router hacía algún equipo en las diferentes redes:

Haga un ping desde el router hacía el switch01:

```
# ping -c 3 192.168.110.1
```

ping router->server

ping router->pc

Haga un ping desde el router hacía el IP-PBX:

```
# ping -c 3 192.168.140.1
```

Ahora verifique el tráfico entre las PCs e Impresoras y los servidores, en caso de que este
permitido, realizar el ping desde el servidor a una PC (ejemplo print server en red pcs).

ping pc->server

pinc server->pc

Ahora pruebe comunicación entre el equipo de administración del IP-PBX.

ping pc->pbx

ping phone->pbx

Pruebas desde la red Inalámbrica:

ping wifi->XXX

Si no ha creado reglas de filtrado de paquetes entre las VLANs entonces no deberá de tener
problemas en las pruebas de conectividad.

## Soporte VLAN Routing, Firewalling, QoS y otros en GNU/Linux

Como mencione al inicio del documento, los sistemas GNU/Linux incluyen el soporte a nivel kernel
2.6+ y las herramientas de usuario para convertir un servidor GNU/Linux en un potente router
estable, seguro y que de un buen desempeño ya que podemos ejecutar un router con GNU/Linux
en equipos x86 tanto en 32 como 64-bit ya sea Intel x_86_64 o AMD64 y recientemente tambien en
sistemas ARM de 32 y 64 bits, ya sean routers pequeños con procesadores atom o geode, o
routers de alto desempeño y disponibilidad con servidores grandes con procesadores 2 quad-core
y 16GB de RAM y fuentes de poder redundantes.

Algunas de las funcionalidades básicas de Networking que podemos realizar en un router GNU/Linux
son:

  * Basic Routing: Static routing.
  * Packet Filtering: Static and Stateful Packet Filtering.
  * Quality of service (QoS).
  * Ethernet Bridges.

En lo personal recomiendo la herramienta [Shorewall](http://shorewall.org/) para realizar la
configuración de sistemas de filtrado de paquetes Firewall tanto para routers de VLANs como
firewalls con conexiones a Internet, permite filtrado de paquetes avanzado basado en zonas,
integración de reglas de enrutado, Balanceo de cargas entre múltiples enlaces de Internet, QoS
y muchas otras funcionalidades.

Algunas otras funcionalidades de Networking avanzadas son:

  * Ethernet Channel Bonding.
  * Router Cluster: VRRP.
  * IDS/IPS: Snort.
  * Virtual Private Network (VPN).
  * Policy Routing.
  * Dynamic Routing: RIP, OSPF, BGP.
  * WAN Load Balancing: Multi WAN.

Una distribución especializada para sistemas de red es [VyOS](http://www.vyos.org/),
basada en Debian soporta todas las funcionalidades básicas y avanzadas antes mencionadas, muy
recomendable para aquellos que vienen del mundo Cisco.

Otro punto clave de las capacidades de Networking de sistemas GNU/Linux es que puede ser
implementado tanto en sistemas físicos en las varias arquitecturas de hardware soportadas por
el kernel Linux y también puede operar en entornos virtualizados como Xen, KVM, VirtualBox,
VMware y hasta HyperV, en ambos casos para arquitecturas 32 y 64-bit, funcionalidades ideales
para entornos en la nuble ya sea pública o privada.

Para aquellos que esten interesados en las capacidades de Networking de Linux es importante
mencionar que incluso hay soporte de interfaces de comunicación para diferentes medios como X.25,
frame relay, ADSL y otros, [Sangoma](http://sangoma.com/) es un fabricante de tarjetas de este
tipo con soporte para Linux.

Para más información se recomienda leer las referencias incluidas al final del documento.

## Recursos adicionales

Si desea obtener más información sobre los programas y páginas de manual relacionadas se
aconseja que acceda a los siguientes recursos adicionales.

### Sitios web

En las paginas listadas a continuación encontrará mayor información sobre los programas utilizados
en este capitulo.

  * [802.1Q - Virtual LANs](http://grouper.ieee.org/groups/802/1/pages/802.1Q.html)
  * [802.1Q VLAN implementation for Linux](http://www.candelatech.com/~greear/vlan.html)
  * [VLANs on Linux](http://www.linuxjournal.com/article/7268)
  * [VLAN - Virtual Local Area Network](http://www.linuxhorizon.ro/vlans.html)
  * [Connecting to an Ethernet 802.1q VLAN](http://linux-ip.net/html/ether-vlan.html)
  * [The Linux Fundation - VLAN](http://www.linuxfoundation.org/collaborate/workgroups/networking/vlan)
  * [InterVLAN Routing](http://www.firewall.cx/vlans-routing.php)
  * [Howto: Configure Linux Virtual Local Area Network (VLAN) (REDHAT WAY)](http://www.cyberciti.biz/tips/howto-configure-linux-virtual-local-area-network-vlan.html)
  * [Linux Advanced Routing & Traffic Control](http://lartc.org/)
  * [Policy Routing With Linux by Matthew G. Marsh](http://www.policyrouting.org/PolicyRoutingBook/)
  * [Shorewall Firewall](http://shorewall.org/)
  * [VyOS - The Universal Router](http://www.vyos.org/)
  * [Sangoma](http://www.sangoma.com/)

### Páginas de manual

Para conocer más sobre el uso y configuración de los programas utilizados en el capitulo,
se recomienda leer los manuales relacionados y el contenido de los archivos de configuración.

  * modprobe (8) - program to add and remove modules from the Linux Kernel
  * lsmod (8) - program to show the status of modules in the Linux Kernel
  * apt-get (8) - APT package handling utility - command-line interface
  * interfaces (5) - network interface configuration for ifup and ifdown
  * vlan-interfaces (5) - (tema desconocido) - vlan extensions for the interfaces(5) file format
  * ifup (8) - bring a network interface up
  * ifdown (8) - take a network interface down
  * ifconfig (8) - configure a network interface
  * ip (8) - show / manipulate routing, devices, policy routing and tunnels
  * tcpdump (8) - dump traffic on a network
  * ping (8) - send ICMP ECHO\_REQUEST to network hosts

