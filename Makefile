#
# Makefile para construir documentos HTML y PDF
#

NOMBRE_ARTICULO="configuracion-interfaz-vlan-trunk-en-linux"

# Realizar todas las operaciones
all: valid clean html pdf

# Validar el archivo indice, este a su vez validará los demas capitulos
valid: dummy
	xmllint --dtdvalid http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd --noout --valid index.xml

# Convertir el documento a HTML y almacenarlo en el directorio "html"
html: valid clean
	xsltproc --output html/index.html \
    --stringparam toc.section.depth 4 \
		--stringparam chunker.output.encoding UTF-8 \
		--stringparam chunker.output.indent yes \
    --stringparam html.stylesheet html.css \
    --stringparam admon.graphics 1 \
    --stringparam navig.graphics 1 \
		/usr/share/xml/docbook/stylesheet/nwalsh/xhtml/onechunk.xsl \
    index.xml
	cp -v css/html.css html/
	cp -v GnuCopyright.htm html/
	cp -vr imagenes html/
	cp -vr /usr/share/xml/docbook/stylesheet/nwalsh/images html/

pdf: dummy
	xmlto fo index.xml
	fop -fo index.fo -pdf pdf/${NOMBRE_ARTICULO}.pdf

clean: dummy
	echo "Limpiando archivos de respaldos, html, pdf"
	find . -name \*~ -exec rm \{\} \;
	rm -rf html/*
	rm -f pdf/*.pdf
	rm -f *.fo

.PHONY: dummy
